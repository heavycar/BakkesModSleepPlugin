#pragma once
#pragma comment(lib, "bakkesmod.lib")

#ifdef _MSC_VER
#pragma warning(disable : 251 244)
#endif
#include "bakkesmod/plugin/bakkesmodplugin.h"
#ifdef _MSC_VER
#pragma warning(default : 251 244)
#endif

#include <random>

namespace SleepPlugin {

class SleepPlugin : public BakkesMod::Plugin::BakkesModPlugin {
  private:
    uint64_t frame;
    uint64_t sleep_max;
    uint64_t sleep_cycle = 0;
    uint64_t sleep_spread = 1;
    std::default_random_engine rng;

  public:
    virtual void onLoad();
    virtual void onUnload();
    void on_draw(CanvasWrapper canvas);
};

} // namespace SleepPlugin