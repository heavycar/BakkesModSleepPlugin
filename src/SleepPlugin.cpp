#include "fmt/format.h"

#include "SleepPlugin.h"
// #ifdef _MSC_VER
// #pragma warning(disable : 251 244)
// #endif
// #include "bakkesmod/wrappers/GameEvent/TutorialWrapper.h"
// #include "bakkesmod/wrappers/GameObject/CarWrapper.h"
// #include "bakkesmod/wrappers/ReplayServerWrapper.h"
// #ifdef _MSC_VER
// #pragma warning(default : 251 244)
// #endif
#include <chrono>
#include <string>
#include <thread>
#include <random>

namespace SleepPlugin {

BAKKESMOD_PLUGIN(SleepPlugin, "Sleep Plugin", "0.3", PLUGINTYPE_SPECTATOR)

void SleepPlugin::onLoad() {
    gameWrapper->RegisterDrawable([&](auto canvas) { on_draw(canvas); });
    cvarManager->registerNotifier(
        "sleep_ms",
        [&](auto args) {
            if (args.size() >= 2) {
                try {
                    auto millis = std::stoull(args[1]);
                    std::this_thread::sleep_for(std::chrono::milliseconds(millis));
                } catch (...) {
                    cvarManager->log(fmt::format("{} is not a valid millisecond count.", args[1]));
                }
            } else {
                std::this_thread::sleep_for(std::chrono::milliseconds(50));
            }
        },
        "Sleeps for x milliseconds.", 1);
    cvarManager->registerNotifier(
        "sleep_rand",
        [&](auto args) {
            if (args.size() >= 2) {
                try {
                    sleep_max = std::stoull(args[1]);
                } catch (...) {
                    cvarManager->log(fmt::format("{} is not a valid millisecond count.", args[1]));
                }
            } else {
                sleep_max = 0;
            }
        },
        "Sleeps for a random period each frame up to x milliseconds.", 1);
    cvarManager->registerNotifier(
        "sleep_cycle",
        [&](auto args) {
            if (args.size() >= 2) {
                try {
                    sleep_cycle = std::stoull(args[1]);
                } catch (...) {
                    cvarManager->log(fmt::format("{} is not a valid millisecond cound.", args[1]));
                }
            } else {
                cvarManager->log(fmt::format("{}", sleep_cycle));
            }
        },
        "Sleeps for x milliseconds every n frames (set via sleep_spread).", 1);
    cvarManager->registerNotifier(
        "sleep_spread",
        [&](auto args) {
            if (args.size() >= 2) {
                try {
                    sleep_spread = std::stoull(args[1]);
                } catch (...) {
                    cvarManager->log(fmt::format("{} is not a valid frame count.", args[1]));
                }
            } else {
                cvarManager->log(fmt::format("{}", sleep_spread));
            }
        },
        "Sets the number of frames to wait between sleeping when sleep_cycle is non-zero.", 1);
}

void SleepPlugin::on_draw(CanvasWrapper canvas) {
    if (sleep_max > 0) {
        auto distribution = std::uniform_int_distribution<uint64_t>(0, sleep_max);
        auto sleep_time = distribution(rng);
        std::this_thread::sleep_for(std::chrono::milliseconds(sleep_time));
    }
    if (sleep_cycle > 0 && frame % sleep_spread == 0) {
        std::this_thread::sleep_for(std::chrono::milliseconds(sleep_cycle));
    }
    frame++;
}

void SleepPlugin::onUnload() {
}

} // namespace SleepPlugin
