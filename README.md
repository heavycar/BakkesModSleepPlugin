# Bakkesmod sleep plugin

## Usage

- `sleep_ms TIME` - Pauses the game thread for `TIME` number of milliseconds.
- `sleep_rand TIME` - Pauses the game thread for a random period up to `TIME` number of milliseconds each frame.