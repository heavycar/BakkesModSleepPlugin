#  BakkesMod_FOUND - system has BakkesMod
#  BakkesMod_INCLUDE_DIRS - the BakkesMod include directories
#  BakkesMod_LIBRARIES - link these to use BakkesMod

include(LibFindMacros)

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(BakkesMod_PKGCONF BakkesMod)

# message(STATUS "BakkesMod_ROOT_DIR: " $ENV{BakkesMod_ROOT_DIR})

find_path(BakkesMod_INCLUDE_DIR
  NAMES bakkesmod/plugin/bakkesmodplugin.h
  PATHS $ENV{BakkesMod_ROOT_DIR}/include
  DOC "The BakkesMod include directory"
)
# message(STATUS "BakkesMod_INCLUDE_DIR: " ${BakkesMod_INCLUDE_DIR})

find_library(BakkesMod_LIBRARY
  NAMES BakkesMod
  PATHS $ENV{BakkesMod_ROOT_DIR}/lib
  DOC "The BakkesMod library"
)
# message(STATUS "BakkesMod_LIBRARY: " ${BakkesMod_LIBRARY})

set(BakkesMod_PROCESS_INCLUDES BakkesMod_INCLUDE_DIR)
set(BakkesMod_PROCESS_LIBS BakkesMod_LIBRARY)
libfind_process(BakkesMod)